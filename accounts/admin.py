from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import DiscordUser

admin.site.register(DiscordUser, UserAdmin)