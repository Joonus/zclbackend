from django.shortcuts import render
from rest_framework import viewsets
from . import serializers, models
# Create your views here.

class DiscordPlayerView(viewsets.ModelViewSet):
    serializer_class = serializers.DiscordUser
    queryset = models.DiscordUser.objects.all()

