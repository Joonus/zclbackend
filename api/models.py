from django.db import models
from accounts.models import DiscordUser

# Create your models here.
EVENT_KEYS = (
    ('player_leave', "Player Leaving"),
    ('bunker_killed', "Bunker Destroyed"),
    ('bunker started', "Bunker Building"),
    ('match_end', "Match Ending"),
    ('bunker_cancelled', "Bunker Cancelled"),
    ('match_start', "Match Starting"),
    ('WIN', 'Player Wins'),
    ('LOSS', 'Player Loses'),
    ('DRAW', 'Player Draws'),
    ('ADJ', 'Player Point Adjustment'),
)

class SC2Profile(models.Model):
    """
    The database object that represents a Starcraft 2 Profile.
    """
    # The primary key is what is known as a handle
    # {region}-S2-{realm_id}-{profile_id}  :str:
    id = models.CharField(primary_key=True, unique=True, max_length=50)
    name = models.CharField(max_length=300)
    profile_url = models.URLField()
    avatar_url = models.CharField(max_length=300)
    profile_id = models.CharField(max_length=100)  # api returns as string.
    region_id = models.IntegerField()
    realm_id = models.IntegerField()

    def __repr__(self):
        return "[id={0.id}, name={0.name}]".format(self)

    def __str__(self):
        return self.__repr__()

class Guild(models.Model):
    """
    A discord Guild
    """
    id = models.BigIntegerField(primary_key=True, unique=True)
    name = models.CharField(max_length=300)


class Replay(models.Model):
    """
    Represents a replay object that a user has uploaded.
    This could be done by a couple ways

        - cli program (an automatic program via websockets -- matchcli)
        - server replay upload
    """
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    # Associate with a match object directly. Only one replay is used per match.
    match = models.OneToOneField('Match', on_delete=models.SET_NULL, null=True)

    # Allow this to be null so we won't lose matches if someone is deleted.
    user = models.ForeignKey(DiscordUser, on_delete=models.SET_NULL, null=True)

    file = models.FileField()

    # Used for quick comparisons of uniqueness
    sha256 = models.CharField(max_length=500, unique=True)

    description = models.TextField(blank=True)


class Match(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    # Each guild can have their own match set
    guild = models.ForeignKey(Guild, on_delete=models.SET_NULL, null=True)


    # A match may belong to a league, or may not. If a league is deleted, we
    # keep the matches
    league = models.ForeignKey('League', on_delete=models.SET_NULL, null=True, related_name='matches')

    # A match may belong to a season, or may not. If a season is deleted, we
    # keep the matches.
    season = models.ForeignKey('Season', on_delete=models.SET_NULL, null=True, related_name='seasons')

    # Since all games are not manually entered, do not allow this to be null
    game_id = models.CharField(max_length=300, unique=True)

    stream_url = models.URLField(blank=True, null=True)



class Roster(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    match = models.ForeignKey('Match', on_delete=models.CASCADE, related_name='rosters')
    sc2_profile = models.ForeignKey('SC2Profile', on_delete=models.CASCADE, related_name='rosters')
    team_number = models.IntegerField()
    position_number = models.IntegerField()
    color = models.CharField(max_length=50, blank=True, default='')

    def __str__(self):
        return "[name={0.sc2_profile.name}, t={0.team_number}, p={0.position_number}".format(self)

    class Meta:
        unique_together = ('match', 'sc2_profile', 'team_number', 'position_number')


class MatchEvent(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    match = models.ForeignKey('Match', on_delete=models.CASCADE, related_name='events')
    handle = models.ForeignKey('SC2Profile', on_delete=models.CASCADE, related_name='match_events')
    opposing_handle = models.ForeignKey('SC2Profile', on_delete=models.SET_NULL, null=True, related_name="+")
    game_time = models.DecimalField(max_digits=12, decimal_places=4, null=True, blank=True)
    value = models.IntegerField(default=0)
    key = models.CharField(max_length=100, choices=EVENT_KEYS)
    raw = models.TextField(blank=True)
    description = models.TextField()
    points = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.key}: {self.handle.name}"

class League(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    # Each guild can have their own league. A league cannot exist without a discord guild.
    # If a guild is deleted, all league objects are deleted with it.
    # We will preserve matches by setting the league to null on the Match table.
    guild = models.ForeignKey(Guild, on_delete=models.CASCADE, related_name='leagues')

    # Owners can come and go. Retain the league if the owner leaves or allow
    # for a transition period.
    owner = models.ForeignKey(DiscordUser, on_delete=models.SET_NULL, null=True)

    name = models.CharField(max_length=300)
    description = models.TextField(blank=True)

    class Meta:
        unique_together = ('guild', 'name',)

class Season(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    # Each season must belong to a league. If a league deletes, cascade.
    league = models.ForeignKey('League', on_delete=models.CASCADE, related_name='seasons')

    name = models.CharField(max_length=300)
    description = models.TextField(blank=True)

    class Meta:
        unique_together = ('league', 'name',)
