from rest_framework import routers
from django.conf.urls import include, url
from . import views

router = routers.DefaultRouter()
router.register('users', views.DiscordPlayerView, base_name='users')

urlpatterns = [
    url('', include(router.urls)),
]
